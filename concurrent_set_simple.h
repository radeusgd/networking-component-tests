/* 
 * File:   concurrent_set_simple.h
 * Author: radek
 *
 * Created on 6 sierpień 2013, 13:53
 */

#ifndef CONCURRENT_SET_SIMPLE_H
#define	CONCURRENT_SET_SIMPLE_H

#include <set>
#include <boost/thread.hpp>

template<typename Data>
class concurrent_set
{
private:
    std::set<Data> the_set;
    mutable boost::mutex the_mutex;
public:
    void insert(const Data& data)
    {
        boost::mutex::scoped_lock lock(the_mutex);
        the_set.insert(data);
    }

    bool empty() const
    {
        boost::mutex::scoped_lock lock(the_mutex);
        return the_set.empty();
    }

    void erase(const Data& data)
    {
        boost::mutex::scoped_lock lock(the_mutex);
        the_set.erase(data);
    }
    
    bool find(const Data& data){
        boost::mutex::scoped_lock lock(the_mutex);
        return (the_set.find(data)!=the_set.end());
    }
    
    class lock{
        boost::mutex::scoped_lock slock;
        std::set<Data>& set;
    public:
        lock(concurrent_set& s) : slock(s.the_mutex), set(s.the_set){}
        std::set<Data>& getSet(){
            return set;
        }
    };
};

#endif	/* CONCURRENT_SET_SIMPLE_H */

