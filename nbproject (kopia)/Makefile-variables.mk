#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=networking-component-tests
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/networking-component-tests
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=networking-component-tests.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/networking-component-tests.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=networking-component-tests
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/networking-component-tests
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=networking-component-tests.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/networking-component-tests.tar
# Debug_Server configuration
CND_PLATFORM_Debug_Server=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug_Server=dist/Debug_Server/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug_Server=networking-component-tests
CND_ARTIFACT_PATH_Debug_Server=dist/Debug_Server/GNU-Linux-x86/networking-component-tests
CND_PACKAGE_DIR_Debug_Server=dist/Debug_Server/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug_Server=networking-component-tests.tar
CND_PACKAGE_PATH_Debug_Server=dist/Debug_Server/GNU-Linux-x86/package/networking-component-tests.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
