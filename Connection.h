/* 
 * File:   Connection.h
 * Author: radek
 *
 * Created on 26 czerwiec 2013, 22:34
 */

#ifndef CONNECTION_H
#define	CONNECTION_H
#include <boost/integer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <vector>
#include <map>
#include <queue>
#include "Packet.h"
#include "SimpleIpAddress.h"
#include "concurrent_queue.h"
#include "concurrent_map.h"
#include "concurrent_set_simple.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

class Connection {
public:
    //typedef void (*callback)(Packet&, void*);
    Connection(const char* ip, unsigned short port);
    bool connect();
    bool isClientIdResolved();
    void send(Packet& packet, bool raw=false);
    void read(bool blocking=true);
    void sendPackets(bool blocking=true);
    unsigned int resendPackets();
    void setCallback(std::function<void(Packet&, void*)> function, void* arguments=NULL);
    void stop();
    virtual ~Connection();
    
    unsigned int waitMsResend=500;
    //void handle_receive_from(const boost::system::error_code& error, size_t bytes_recvd);
    //void handle_send_to(const boost::system::error_code& /*error*/, size_t /*bytes_sent*/);
protected:
    const char* serverIp;
    unsigned int serverPort;
    boost::uint16_t clientId;
    //boost::asio::io_service io_service;
    //boost::asio::io_service::work worker;
    int sockfd;
    //SimpleIpAddress endpoint;
    //boost::uint8_t* _data;udp::endpoint _sender;
    boost::mutex currentIdMutex;
    concurrent_queue<Packet> packetsToSend;
    
    //boost::int16_t confirmations;
    concurrent_set<boost::uint16_t> confirmations;//std::map<boost::uint16_t, bool> confirmations;
    boost::uint16_t latestConfirmedMessage;
    concurrent_map<boost::uint16_t, Packet> confirmablePackets;
    boost::uint16_t currentId;
    boost::posix_time::ptime lastMessageTime;
    void sendConfirmation(boost::uint16_t confirmedId);
    void clearOldConfirmations();
    
    std::function<void(Packet&, void*)> onPacket; void* args;
};

#endif	/* CONNECTION_H */

