/* 
 * File:   ChatApp.h
 * Author: radek
 *
 * Created on 2 lipiec 2013, 11:57
 */

#ifndef CHATAPP_H
#define	CHATAPP_H

#include <gtk/gtk.h>
#include "Connection.h"

class ChatApp {
public:
    ChatApp(int* argc, char*** argv);
    void run();
    virtual ~ChatApp();
//private:
    GtkWidget *window;
    struct ConnectionForm{
        void init(ChatApp* app);
        void onClickLogin();
        
        GtkWidget* container;
        GtkWidget* ipEntry;
        GtkWidget* portEntry;
        GtkWidget* nickEntry;
        GtkWidget* statusLabel;
        
        GtkWidget* button;
        ChatApp* app;
        boost::thread* timeout=NULL;

    } connection_form;
    struct ChatWidget{
        void init(ChatApp* app);
        GtkWidget* container;
        GtkWidget* textInput;
        GtkWidget* textArea;
        GtkWidget* scrolledWindow;
        ChatApp* app;
        
        std::map<boost::posix_time::ptime, std::string> messages;
        void updateMessages();
        void onClickSend();
        
        boost::thread* pinger;
    
    } chat;
    Connection* connection;
    boost::thread* reader=NULL;
    boost::thread* sender=NULL;
    bool loggedIn;
};

#endif	/* CHATAPP_H */

