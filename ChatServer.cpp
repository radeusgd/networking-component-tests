/* 
 * File:   ChatServer.cpp
 * Author: radek
 * 
 * Created on 1 lipiec 2013, 17:18
 */

#include "ChatServer.h"

ChatClient::ChatClient(SimpleIpAddress endpoint, boost::uint16_t clientId) : BaseClient(endpoint, clientId) {}

ChatServer::ChatServer(unsigned short port) : BaseServerConnection(port) {
    waitMsResend=700;
    waitMsDisconnect=1000*60;
}

ChatServer::~ChatServer() {
}

/*
 * 
 *      List of message ids:
 *              0 - login, login confirmation/failed
 *              1 - error with text, maybe later id?
 *              2 - message (nick, text)
 *
 */

bool ChatServer::processPacket(ChatClient* client, Packet& packet){
    //std::cout<<"Packet processing\n";// from "<<client->endpoint<<"\n";
    DataPacket message = packet;
    if(client->clientId==0){//client wasn't connected before
        boost::uint8_t msgId = message.getUint8();
        if(msgId==0){//"login" message
            std::string nick = message.getString();
            if(true/*TODO test if nick is occupied, if yes, refuse to log in*/){
                
                client->nickname = nick;//it should work, 'cause it's a pointer
                
                DataPacket response;
                /*boost::uint16_t newId = getNextFreeId();
                if(newId==0){
                    ProtocolPacket error;
                    error.setType(ProtocolPacket::SERVER_FULL);
                    send(client, error, false);
                    return false;
                }
                ((ProtocolPacket)response).setType(ProtocolPacket::CLIENTID);
                client->clientId=newId;*/
                response.confirmation_needed=true;
                response.putUint8(0);
                response.putUint8(1);//true that login succeed, it's because i don't know what to send here, it's very simple login
                /*if(send(client,response))
                    std::cout<<"Logged in succesfully\n";
                else
                    std::cout<<"Login message recvd but cannot send confirmation (errno "<<errno<<")\n";*/
                send(client,response);
                return true;
            }
            else{
                DataPacket response;
                response.confirmation_needed=true;
                response.putUint8(1);
                response.putString("Nickname is taken! Choose another one");
                send(client,response);
                std::cout<<"Nickname in use\n";
                return false;
            }
        }
        else{
            DataPacket response;
            response.putUint8(1);
            response.putString("You are not logged in!");
            send(client,response);
            std::cout<<"Unknown message "<<(int)msgId<<" (not logged in)\n";
            return false;
        }
    }
    else{//remembered client
        char messageId = message.getUint8();
        switch(messageId){
            case 2:
            {
                DataPacket broadcast;
                broadcast.confirmation_needed=true;
                broadcast.putUint8(2);
                broadcast.putString(client->nickname);
                broadcast.putTimeStamp(message.getTimeStamp());
                std::string txtMessage = message.getString();
                broadcast.putString(txtMessage);
                sendToAll(broadcast);
                std::cout<<client->nickname<<": "<<txtMessage<<"\n";
            }
            case 3:
            {
                DataPacket ping;
                ping.putUint8(3);
                send(client,ping);
            }
            break;
            default:
            {
                std::cout<<"Unknown message - "<<(int)messageId<<"\n";
                return false;
            }
                
        }
        return true;
    }
}

void ChatServer::sendToAll(Packet& packet){
    for(concurrent_map<boost::uint16_t,ChatClient*>::iterator i=clients.begin();i!=clients.end();i++){
        //TODO you don't need to know anything about ChatClient, it's broadcast, so use only endpoint
        send(i->second,packet);
    }
}