/* 
 * File:   SimpleIpAddress.cpp
 * Author: radek
 * 
 * Created on 28 lipiec 2013, 21:01
 */

#include <stddef.h>

#include "SimpleIpAddress.h"
#include <boost/integer.hpp>
#include <string.h>

bool SimpleIpAddress::operator <(const SimpleIpAddress& other) const{
    if(socketAddress==nullptr or socketAddress==(void*)0x1){
        if(other.socketAddress==nullptr)
            return false;
        return true;
    }
    if(other.socketAddress==nullptr or socketAddress==(void*)0x1)
        return false;
    for(boost::uint8_t i=0;i<14;i++)
        if(socketAddress->sa_data[i]<other.socketAddress->sa_data[i])
            return true;
        else if(socketAddress->sa_data[i]<other.socketAddress->sa_data[i])
            return false;
    return false;
}

bool SimpleIpAddress::operator >(const SimpleIpAddress& other) const{
    if(socketAddress==nullptr)
        return false;
    if(other.socketAddress==nullptr)
        return true;
    for(boost::uint8_t i=0;i<14;i++)
        if(socketAddress->sa_data[i]>other.socketAddress->sa_data[i])
            return true;
        else if(socketAddress->sa_data[i]>other.socketAddress->sa_data[i])
            return false;
    return false;
}

bool SimpleIpAddress::operator ==(const SimpleIpAddress& other) const{
    if(socketAddress==nullptr or other.socketAddress==nullptr)
        return false;
    if(socketAddress->sa_family!=other.socketAddress->sa_family)
        return false;
    for(boost::uint8_t i=0;i<14;i++)
        if(socketAddress->sa_data[i]!=other.socketAddress->sa_data[i])
            return false;
    return true;
}

void SimpleIpAddress::operator =(const SimpleIpAddress& other){
    if(socketAddress!=nullptr)
        delete socketAddress;
    if(other.socketAddress!=nullptr){
        socketAddress = new sockaddr();
        memcpy(socketAddress,other.socketAddress,sizeof(sockaddr));
    }
    else{
        socketAddress=nullptr;
    }
}

SimpleIpAddress::SimpleIpAddress(){
    socketAddress=nullptr;
}

SimpleIpAddress::SimpleIpAddress(const SimpleIpAddress& other){
    if(other.socketAddress!=nullptr){
        socketAddress = new sockaddr();
        memcpy(socketAddress,other.socketAddress,sizeof(sockaddr));
    }
    else{
        socketAddress=nullptr;
    }
}

SimpleIpAddress::SimpleIpAddress(const sockaddr* socketAddr){
    if(socketAddr!=nullptr){
        socketAddress = new sockaddr();
        memcpy(socketAddress,socketAddr,sizeof(sockaddr));
    }
    else{
        socketAddress=nullptr;
    }
}

sockaddr* SimpleIpAddress::getSocketAddress(){
    return socketAddress;
}

SimpleIpAddress::~SimpleIpAddress(){
    if(socketAddress!=nullptr)
        delete socketAddress;
}
