/* 
 * File:   ProtocolPacket.h
 * Author: radek
 *
 * Created on 27 czerwiec 2013, 21:01
 */

#ifndef PROTOCOLPACKET_H
#define	PROTOCOLPACKET_H
#include "DataPacket.h"
class ProtocolPacket  : public DataPacket{
public:
    
    ProtocolPacket();
    ProtocolPacket(boost::uint8_t* data);
    ProtocolPacket(const Packet& orig);
    virtual ~ProtocolPacket();
    enum Type {
        CONFIRMATION=1,
        CLIENTID=2,
        SERVER_FULL=3
    };
    void setType(Type t);
    Type getType();
private:
};

#endif	/* PROTOCOLPACKET_H */

