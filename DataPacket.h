/* 
 * File:   DataPacket.h
 * Author: radek
 *
 * Created on 27 czerwiec 2013, 20:15
 */

#ifndef DATAPACKET_H
#define	DATAPACKET_H
#include "Packet.h"
#include <string>
class DataPacket : public Packet{
public:
    DataPacket();
    DataPacket(boost::uint8_t* data);
    DataPacket(const Packet& orig);
    virtual ~DataPacket();

    void putInt8(boost::int8_t x);
    void putInt16(boost::int16_t x);
    void putInt32(boost::int32_t x);
    void putUint8(boost::uint8_t x);
    void putUint16(boost::uint16_t x);
    void putUint32(boost::uint32_t x);
    void putFloat(float x);
    void putDouble(double x);
    
    boost::uint8_t getInt8();
    boost::int16_t getInt16();
    boost::int32_t getInt32();
    boost::uint8_t getUint8();
    boost::uint16_t getUint16();
    boost::uint32_t getUint32();
    float getFloat();
    double getDouble();
    
    void putString(std::string s);
    std::string getString();
    
    void putTimeStamp(boost::posix_time::ptime time);
    boost::posix_time::ptime getTimeStamp();
//    template<class T> void put(T){
//        boost::uint8_t* value = (boost::uint8_t*)&x;
//        if(Packet::is_big_endian())
//            for(char i=0;i<sizeof(T);i++)
//                data.push_back(value[i]);
//        else
//            for(char i=sizeof(T)-1;i>=0;i--)
//                data.push_back(value[i]);
//        cursor+=sizeof(T);
//    };

    
protected:
    unsigned short cursor;
};

#endif	/* DATAPACKET_H */

