/* 
 * File:   ServerConnection.h
 * Author: radek
 *
 * Created on 27 czerwiec 2013, 10:39
 */

#ifndef SERVERCONNECTION_H
#define	SERVERCONNECTION_H
#include "Packet.h"
#include "ProtocolPacket.h"
#include <boost/thread.hpp>
#include "concurrent_map.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

//#ifdef DEBUG
#include <iostream>
//#endif

//using boost::asio::ip::udp;

#include "SimpleIpAddress.h"
#include "concurrent_queue.h"
#include "concurrent_set_simple.h"

class BaseClient{
public:
    BaseClient(SimpleIpAddress& sender, boost::uint16_t clientId);
    
    SimpleIpAddress endpoint;
    boost::uint16_t clientId;
    
    boost::posix_time::ptime lastMessageTime;
    
    concurrent_queue<Packet> packetsToSend;
    
    //boost::int16_t confirmations;
    //concurrent_map<boost::uint16_t, bool> confirmations;
    concurrent_set<boost::uint16_t> confirmations;
    
    boost::uint16_t latestConfirmedMessage;
    concurrent_map<boost::uint16_t, Packet> confirmablePackets;
    boost::uint16_t currentId;
};

struct SocketBroken{
};

template <class Client> class BaseServerConnection {
public:
    BaseServerConnection(unsigned short port) : port(port){
//        socket.open(boost::asio::ip::udp::v4());
//        socket.bind(SimpleIpAddress(boost::asio::ip::udp::v4(),port));
    }
    
    boost::uint16_t getNextFreeId(){
        if(tmpClientId>65300){//TODO if too high try to look in map for free places
            return 0;
        }
        tmpClientId++;
        return tmpClientId;
    }
    
    void send(Client* client, Packet& packet, bool raw=false){
        if(packet.confirmation_needed and !raw){
            packet.id=client->currentId;
            if(client->currentId>65000)
                client->currentId=0;
            client->currentId=client->currentId+1;
            client->confirmablePackets[packet.id]=packet;
        }
        packet.setClientId(client->clientId);
        //if(packet.sendTime == boost::posix_time::neg_infin)
            packet.sendTime=boost::posix_time::microsec_clock::local_time();//set new send time
        ClientPacket cp;
        cp.client=client;
        cp.packet=packet;
        packetsToSend.push(cp);
        //std::cout<<cp.packet.id<<" ";
        /*if(sendto(sockfd,
                packet.getBytes(),
                packet.getHeaderLength()+packet.getLength(),
                0,
                client->endpoint.getSocketAddress(),
                sizeof(sockaddr))==packet.getHeaderLength()+packet.getLength())
            return true;
        return false;*/
          
    }
    virtual bool listen(unsigned short nThreads=1){//TODO if is 1 it's blocking, if more it's non blocking
        running=true;
        boost::uint8_t* _data = nullptr;
        //currently function blocks and uses 1 thread
        addrinfo hints, *res;
        // get host info, make socket, bind it to port
        memset(&hints, 0, sizeof (hints));//hmm, is it still needed in C++? I don't think so, but I'll leave it like this by now
        hints.ai_family = AF_INET;//AF_UNSPEC;  // use IPv4 or IPv6, whichever
        hints.ai_socktype = SOCK_DGRAM;
        hints.ai_flags = AI_PASSIVE;
            std::stringstream s;
            s<<port;
        getaddrinfo(nullptr, s.str().c_str() , &hints, &res);
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if(bind(sockfd, res->ai_addr, res->ai_addrlen)==-1){
            running=false;
            return false;
        }
        boost::thread sender([this](){
            try{
            while(running){
                sendPackets(true);
                removeInactiveClients();
                resendPackets();
                boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            }
            }
            catch(...){}
        });  
        // no need to accept(), just recvfrom():
        while(running){
            int byte_count;
            sockaddr addr;
            SimpleIpAddress _sender;
            //char ipstr[INET6_ADDRSTRLEN];
            socklen_t fromlen = sizeof(addr);
            if(_data!=nullptr){
                delete [] _data;
                _data=nullptr;
            }
            _data = new boost::uint8_t[Packet::max_length*2];
            byte_count = recvfrom(sockfd, _data, Packet::max_length*2, 0, &addr, &fromlen);
            _sender = SimpleIpAddress(&addr);
            if(byte_count==-1){
                printf("Error in recvfrom, errno %i\n",errno);
                continue;
            }
            //printf("recv()'d %d bytes of data in buf\n", byte_count);
            /*printf("from IP address %s\n",
                inet_ntop(addr.ss_family,
                    (addr.ss_family == AF_INET ?
                        ((sockaddr_in *)&addr)->sin_addr:
                        ((sockaddr_in6 *)&addr)->sin6_addr),
                    ipstr, sizeof(ipstr));*/
            ////////////////////////////////////////////////////////////////////process data
            
            if(byte_count<Packet::getMinimalHeaderLength()){
                printf("Packet invalid (doesn't contain header)\n");
                continue;
            }
            if(byte_count<Packet::getLengthOfPacket(_data)){
                printf("Packet invalid (not all data received, should be %i)\n",Packet::getLengthOfPacket(_data));
                continue;
            }
            Packet received_packet(_data);
            boost::uint16_t senderId = received_packet.getClientId();
            delete [] _data;
            _data=nullptr;
            Client* current;
            //TODO if clientid!=0 check if IP is correct, if not, revoke message
            if(senderId!=0 and clients.find(senderId)!=clients.end()){
                current = clients[senderId];
                if(((ProtocolPacket)received_packet).getType()==ProtocolPacket::CONFIRMATION){
                ProtocolPacket confirmation = received_packet;
                //current->confirmablePackets.erase(confirmation.getUint32());
                boost::int16_t latestConfirmed = confirmation.getUint16();
                current->confirmablePackets.erase(latestConfirmed);
                boost::int16_t r_confirmations=confirmation.getUint16();
                for(char i=0;i<16;i++){
                    if(r_confirmations&(1<<i)){
                        current->confirmablePackets.erase(latestConfirmed-i);
                    }
                    else{
                        if(current->confirmablePackets.find(latestConfirmed-i)!=current->confirmablePackets.end()){
                            send(current, current->confirmablePackets[latestConfirmed-i], true);
                            /*sendto(sockfd,
                                    current->confirmablePackets[latestConfirmed-i].getBytes(),
                                    current->confirmablePackets[latestConfirmed-i].getHeaderLength()+current->confirmablePackets[latestConfirmed-i].getLength(),
                                    0,
                                    current->endpoint.getSocketAddress(),
                                    sizeof(sockaddr));*/
                        }
                    }
                }
                current->lastMessageTime = boost::posix_time::second_clock::local_time();
                continue;
                }
            }
            else{
                current = new Client(_sender, senderId);
                //std::cout<<"New client! ("<<clients.size()<<")\n";
                current->lastMessageTime = boost::posix_time::second_clock::local_time();
            }
            if(received_packet.confirmation_needed){
                if(current->confirmations.find(received_packet.id)){//if(current->confirmations[received_packet.id]==true){
                    //std::cout<<"Repeated packet "<<(int)received_packet.id<<"\n";
                    continue;
                }
                sendConfirmation(current, received_packet.id);
            }
//            if(senderId==0){
//                current->clientId=getNextFreeId();
//            }
            if(processPacket(current, received_packet)){
                if(senderId==0/* and current->clientId!=0*/){
                    current->clientId=getNextFreeId();
                    if(current->clientId!=0)
                        clients[current->clientId]=current;
                }
                current->lastMessageTime = boost::posix_time::second_clock::local_time();
            }
            else if(senderId==0){
                clients.erase(current->clientId);
                delete current;
                tmpClientId--;//TODO remove client and free ID
            }//TODO remove client after timeout
//                clients[senderId]=current;
//                if(clients[_sender]!=current)
//                    clients.insert(std::pair<SimpleIpAddress,Client*>(_sender,current));
                //current->lastMessageTime = boost::posix_time::second_clock::local_time();
            
            ////////////////////////////////////////////////////////////////////end processing
        }
        sender.interrupt();
        close(sockfd);
        return true;
    }
    virtual bool processPacket(Client* client, Packet& packet){
        
    }
    unsigned short getPort(){
        return port;//TODO what if port was 0 and had to be chosen by system, handle it in future
    }
    
    void sendConfirmation(Client* current, boost::uint16_t confirmedId){
        ProtocolPacket confirmation;
        confirmation.setType(ProtocolPacket::CONFIRMATION);
        //confirmation.putUint32(confirmedId);
        //current->confirmations[confirmedId]=true;
        current->confirmations.insert(confirmedId);
        {//update confirmations
            if(confirmedId>=current->latestConfirmedMessage){
                current->latestConfirmedMessage = confirmedId;
            }
        }
        confirmation.putUint16(confirmedId);
        //confirmation.putInt16(current->confirmations);
        boost::uint16_t confirms=0;
        for(int i=0;i<16;i++)
            if(current->confirmations.find(confirmedId-i))//if(current->confirmations[confirmedId-i])
                confirms|=(1<<i);
        confirmation.putUint16(confirms);
        send(current, confirmation);
        clearOldConfirmations(current);
    }
    void stop(){
        if(running){
            running=false;
            //io_service.stop();
            for(std::vector<boost::thread*>::iterator i = threads.begin();i!=threads.end();i++){
                (*i)->join();
                delete *i;
            }
            threads.clear();
        }
    }
    unsigned int connectedClients(){
        return clients.size();
    }
    virtual ~BaseServerConnection(){stop();}
    int waitMsResend=500;
    int waitMsDisconnect=5000;
    //unsigned int resentPackets=0;//TODO debug only
protected:
    //boost::asio::io_service io_service;
    //boost::asio::io_service::work worker;
    int sockfd;//udp::socket socket;
    //boost::uint8_t* _data;// SimpleIpAddress _sender;
    concurrent_map<boost::uint16_t, Client*> clients;
    boost::uint16_t tmpClientId=1;
    unsigned int port;
    bool running;
    std::vector<boost::thread*> threads;
    //boost::asio::io_service::work worker;
    struct ClientPacket{
        Client* client;
        Packet packet;
    };
    concurrent_queue<ClientPacket> packetsToSend;
    void sendPackets(bool blocking=true){
        if(packetsToSend.empty())
            return;
        do{
            Packet p=packetsToSend.front().packet;
            int length=p.getHeaderLength()+p.getLength();
            boost::uint8_t *bytes = p.getBytes();
            if(sendto(sockfd,
                bytes,
                length,
                0,
                packetsToSend.front().client->endpoint.getSocketAddress(),
                sizeof(sockaddr))==length)
                    packetsToSend.pop();
            delete [] bytes;
            boost::this_thread::sleep(boost::posix_time::microseconds(15));
        }while(running and blocking and !packetsToSend.empty());
    }
    void resendPackets(){
        boost::posix_time::time_duration waiter = boost::posix_time::milliseconds(waitMsResend);
        //boost::posix_time::time_duration maxKeepTime = boost::posix_time::seconds(10);
        typename concurrent_map<boost::uint16_t, Client*>::iterator clientsbegin = clients.begin();
        typename concurrent_map<boost::uint16_t, Client*>::iterator clientsend = clients.end();
        typename concurrent_map<boost::uint16_t, Client*>::lock lock(clients);
        boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
        for(typename concurrent_map<boost::uint16_t, Client*>::iterator c=clientsbegin;c!=clientsend;c++){
            for(concurrent_map<boost::uint16_t,Packet>::iterator i=c->second->confirmablePackets.begin();i!=c->second->confirmablePackets.end();i++){
                boost::posix_time::time_duration duration = (now)-(i->second.sendTime);
                if(duration>waiter){
                    send(c->second, i->second, true);
                    /*resentPackets++;
                    if(resentPackets%50==0){
                        std::cout<<"Resent "<<resentPackets<<" packets\n";
                    }*/
                    //if(/*duration != boost::posix_time::pos_infin and */duration>maxKeepTime){
                    //    c->second->confirmablePackets.erase(i);
                    //}
                }
                if((signed long int)c->second->currentId-5000>i->second.id  or (c->second->currentId>9000 and c->second->currentId<32000 and i->second.id>40000))//packet too old, give up
                    c->second->confirmablePackets.erase(i);
            }
        }
    }
    void removeInactiveClients(){
        //std::cout<<"Remover\n";
        boost::posix_time::time_duration timeout = boost::posix_time::milliseconds(waitMsDisconnect);
        typename concurrent_map<boost::uint16_t, Client*>::iterator clientsbegin = clients.begin();
        typename concurrent_map<boost::uint16_t, Client*>::iterator clientsend = clients.end();
        std::list<typename concurrent_map<boost::uint16_t, Client*>::iterator> toRemove;
        {
            typename concurrent_map<boost::uint16_t, Client*>::lock lock(clients);
            boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
            for(typename concurrent_map<boost::uint16_t, Client*>::iterator c=clientsbegin;c!=clientsend;c++){
                boost::posix_time::time_duration duration = (now)-(c->second->lastMessageTime);
                if(duration>timeout){
                    //clients.erase(c);
                    toRemove.push_back(c);
                }
                //std::cout<<now<<" "<<(c->second->lastMessageTime)<<" "<<duration<<"("<<c->second->clientId<<")\n";
            }
        }
        for(auto i : toRemove){
            printf("Removed client %d\n",i->second->clientId);
            clients.erase(i);
        }
    }
    
    void clearOldConfirmations(Client* current){
        concurrent_set<boost::uint16_t>::lock lock(current->confirmations);
        for(auto i : lock.getSet()){
            if(i<current->latestConfirmedMessage-10000 or (i>40000 and current->latestConfirmedMessage>9000 and current->latestConfirmedMessage<32000))
                i=lock.getSet().erase(i);
        }
    }
};

#endif	/* SERVERCONNECTION_H */

