/* 
 * File:   DataPacket.cpp
 * Author: radek
 * 
 * Created on 27 czerwiec 2013, 20:15
 */

#include "DataPacket.h"
#include "string.h"

DataPacket::DataPacket() : Packet(), cursor(0){}
DataPacket::DataPacket(boost::uint8_t* data) : Packet(data), cursor(0){}
DataPacket::DataPacket(const Packet& orig) : Packet(orig), cursor(0){}
DataPacket::~DataPacket() {}

void DataPacket::putInt8(boost::int8_t x){
    boost::uint8_t* y=(boost::uint8_t*)&x;
    data.push_back(*y);
    cursor++;
}
void DataPacket::putInt16(boost::int16_t x){  
    boost::uint8_t* y=(boost::uint8_t*)&x;
    if(Packet::is_lil_endian()){
        data.push_back(y[0]);
        data.push_back(y[1]);
    }
    else
    {
        data.push_back(y[1]);
        data.push_back(y[0]);
    }
    cursor+=2;
}
void DataPacket::putInt32(boost::int32_t x){  
    boost::uint8_t* y=(boost::uint8_t*)&x;
    if(Packet::is_lil_endian()){
        data.push_back(y[0]);
        data.push_back(y[1]);
        data.push_back(y[2]);
        data.push_back(y[3]);
    }
    else
    {
        data.push_back(y[3]);
        data.push_back(y[2]);
        data.push_back(y[1]);
        data.push_back(y[0]);
    }
    cursor+=4;
}
void DataPacket::putUint8(boost::uint8_t x){
    data.push_back(x);
    cursor++;
}
void DataPacket::putUint16(boost::uint16_t x){ 
    boost::uint8_t* y=(boost::uint8_t*)&x;
    if(Packet::is_lil_endian()){
        data.push_back(y[0]);
        data.push_back(y[1]);
    }
    else
    {
        data.push_back(y[1]);
        data.push_back(y[0]);
    }
    cursor+=2;
}
void DataPacket::putUint32(boost::uint32_t x){  
    boost::uint8_t* y=(boost::uint8_t*)&x;
    if(Packet::is_lil_endian()){
        data.push_back(y[0]);
        data.push_back(y[1]);
        data.push_back(y[2]);
        data.push_back(y[3]);
    }
    else
    {
        data.push_back(y[3]);
        data.push_back(y[2]);
        data.push_back(y[1]);
        data.push_back(y[0]);
    }
    cursor+=4;
}
void DataPacket::putFloat(float x){  
    boost::uint8_t* y=(boost::uint8_t*)&x;
    if(Packet::is_lil_endian()){
        data.push_back(y[0]);
        data.push_back(y[1]);
        data.push_back(y[2]);
        data.push_back(y[3]);
    }
    else
    {
        data.push_back(y[3]);
        data.push_back(y[2]);
        data.push_back(y[1]);
        data.push_back(y[0]);
    }
    cursor+=4;
}
void DataPacket::putDouble(double x){  
    boost::uint8_t* y=(boost::uint8_t*)&x;
    if(Packet::is_lil_endian()){
        data.push_back(y[0]);
        data.push_back(y[1]);
        data.push_back(y[2]);
        data.push_back(y[3]);
        data.push_back(y[4]);
        data.push_back(y[5]);
        data.push_back(y[6]);
        data.push_back(y[7]);
    }
    else
    {
        data.push_back(y[7]);
        data.push_back(y[6]);
        data.push_back(y[5]);
        data.push_back(y[4]);
        data.push_back(y[3]);
        data.push_back(y[2]);
        data.push_back(y[1]);
        data.push_back(y[0]);
    }
    cursor+=8;
}

boost::uint8_t DataPacket::getInt8(){  
    if(cursor>(int)data.size()-1)
        return 0;//TODO exception??
    boost::uint8_t *x = &data[cursor];
    cursor++;
    return *x;
}
boost::int16_t DataPacket::getInt16(){
    if(cursor>(int)data.size()-2)
        return 0;
    boost::uint8_t x[2];
    if(Packet::is_lil_endian()){
        x[0]=data[cursor];
        x[1]=data[cursor+1];
    }else{
        x[0]=data[cursor+1];
        x[1]=data[cursor];
    }
    cursor+=2;
    boost::int16_t r;
    memcpy(&r,&x,sizeof(r));
    return r;
}
boost::int32_t DataPacket::getInt32(){  
    if(cursor>(int)data.size()-4)
        return 0;
    boost::uint8_t x[4];
    if(Packet::is_lil_endian()){
        x[0]=data[cursor];
        x[1]=data[cursor+1];
        x[2]=data[cursor+2];
        x[3]=data[cursor+3];
    }else{
        x[0]=data[cursor+3];
        x[1]=data[cursor+2];
        x[2]=data[cursor+1];
        x[3]=data[cursor];
    }
    cursor+=4;
    boost::int32_t r;
    memcpy(&r,&x,sizeof(r));
    return r;
}
boost::uint8_t DataPacket::getUint8(){
    if(cursor>(int)data.size()-1)
        return 0;
    boost::uint8_t* x = &data[cursor];
    cursor++;
    return *x;
}
boost::uint16_t DataPacket::getUint16(){  
    if(cursor>(int)data.size()-2)
        return 0;
    boost::uint8_t x[2];
    if(Packet::is_lil_endian()){
        x[0]=data[cursor];
        x[1]=data[cursor+1];
    }else{
        x[0]=data[cursor+1];
        x[1]=data[cursor];
    }
    cursor+=2;
    boost::uint16_t r;
    memcpy(&r,&x,sizeof(r));
    return r;
}
boost::uint32_t DataPacket::getUint32(){
    if(cursor>(int)data.size()-4)
        return 0;
    boost::uint8_t x[4];
    if(!Packet::is_lil_endian()){
        x[0]=data[cursor];
        x[1]=data[cursor+1];
        x[2]=data[cursor+2];
        x[3]=data[cursor+3];
    }else{
        x[0]=data[cursor+3];
        x[1]=data[cursor+2];
        x[2]=data[cursor+1];
        x[3]=data[cursor];
    }
    cursor+=4;
    boost::uint32_t r;
    memcpy(&r,&x,sizeof(r));
    return r;
}
float DataPacket::getFloat(){
    if(cursor>(int)data.size()-4)
        return 0;
    boost::uint8_t x[4];
    if(Packet::is_lil_endian()){
        x[0]=data[cursor];
        x[1]=data[cursor+1];
        x[2]=data[cursor+2];
        x[3]=data[cursor+3];
    }else{
        x[0]=data[cursor+3];
        x[1]=data[cursor+2];
        x[2]=data[cursor+1];
        x[3]=data[cursor];
    }
    cursor+=4;
    float r;
    memcpy(&r,&x,sizeof(float));
    return r;
}
double DataPacket::getDouble(){  
    if(cursor>(int)data.size()-8)
        return 0;
    boost::uint8_t x[8];
    if(Packet::is_lil_endian()){
        x[0]=data[cursor];
        x[1]=data[cursor+1];
        x[2]=data[cursor+2];
        x[3]=data[cursor+3];
        x[4]=data[cursor+4];
        x[5]=data[cursor+5];
        x[6]=data[cursor+6];
        x[7]=data[cursor+7];
    }else{
        x[0]=data[cursor+7];
        x[1]=data[cursor+6];
        x[2]=data[cursor+5];
        x[3]=data[cursor+4];
        x[4]=data[cursor+3];
        x[5]=data[cursor+2];
        x[6]=data[cursor+1];
        x[7]=data[cursor];
    }
    cursor+=8;
    double r;
    memcpy(&r,&x,sizeof(double));
    return r;
}

std::string DataPacket::getString(){//TODO NULL-terminated
    //boost::uint16_t length = getUint16();
    std::string result;
    while(true){//for(boost::uint16_t i=0;i<length;i++){
        char c = (char)getInt8();
        if(c==0)
            return result;
        result+=c;
    }
    //return result;
}

void DataPacket::putString(std::string s){
    //putUint16(s.length());
    for(boost::uint16_t i=0;i<s.length();i++){
        putInt8(s[i]);
    }
    putInt8(0);
}

void DataPacket::putTimeStamp(boost::posix_time::ptime time){
    putString(boost::posix_time::to_iso_string(time));
}

boost::posix_time::ptime DataPacket::getTimeStamp(){
    try{
    std::string str = getString();
    return boost::posix_time::from_iso_string(str);
    }
    catch(...){
        return boost::posix_time::not_a_date_time;
    }
}