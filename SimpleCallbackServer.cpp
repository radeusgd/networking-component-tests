/* 
 * File:   CallbackServer.cpp
 * Author: radek
 * 
 * Created on 27 czerwiec 2013, 19:45
 */

#include "SimpleCallbackServer.h"
#include "BaseServerConnection.h"

SimpleCallbackServer::SimpleCallbackServer(std::function<bool(SimpleCallbackServer*, BaseClient*, Packet&)> onMessage, unsigned short port) : BaseServerConnection<BaseClient>(port){ 
    processCall=onMessage;
}

SimpleCallbackServer::~SimpleCallbackServer() {
}

bool SimpleCallbackServer::processPacket(BaseClient* client, Packet& packet){
    return processCall(this, client,packet);
}