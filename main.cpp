/* 
 * File:   main.cpp
 * Author: radek
 *
 * Created on 27 czerwiec 2013, 19:58
 */
#define DEBUG
#include <iostream>
#include <string>

#include "ChatServer.h"
#include "Connection.h"
#include "ChatApp.h"
#include "SimpleCallbackServer.h"

/*
 * 
 */
int main(int argc, char** argv) {
    bool isServer = argc>1 ? (std::string(argv[1])=="server") : false; //false
    bool isEncoder = argc>1 ? (std::string(argv[1])=="encode") : false;
    bool isFloodOut = argc>1 ? (std::string(argv[1])=="floodout") : false;
    bool isFloodIn = argc>1 ? (std::string(argv[1])=="floodin") : false;
    if(isServer){
        try{
            ChatServer server(9876);
            std::cout<<"Listening...\n";
            if(!server.listen(0))
                std::cout<<"Listening interrupted abnormally (maybe cannot bind port?)\n";
            else
                std::cout<<"Listening finished after stopping server\n";
        }
        catch(boost::system::system_error e){
            std::cout<<"Error: "<<e.what()<<"\nStopping\n";
        }
        catch(SocketBroken){
            std::cout<<"Socket is broken. Stopping\n";
        }
    }
    else if(isEncoder){
        int errors=0;
        bool confirm=false;
        if(argc>2 and argv[2]==std::string("confirm"))
            confirm=true;
        for(int i=0;i<100;i++){
            DataPacket* x=(new DataPacket());
            DataPacket a = *x;//not deleting because I want leaks here
            a.setClientId(i);
            a.id=5000-i;
            a.confirmation_needed=confirm;
            a.putInt32(9876);
            a.putUint16(1234);
            a.putDouble(0.024035);
            a.putString("Test");
            boost::posix_time::ptime time = boost::posix_time::microsec_clock::local_time();
            a.putTimeStamp(time);
            boost::uint8_t* bytes = /*(char*)*/((Packet)a).getBytes();
            for(int i=0;i<a.getLength()+a.getHeaderLength();i++)
                std::cout<<(int)bytes[i]<<" ";
            std::cout<<std::endl;
            Packet b((boost::uint8_t*)bytes);
            DataPacket c = b;
            if(a.getHeaderLength()+a.getLength()!=c.getHeaderLength()+c.getLength()){
                std::cout<<"Lengths don't match\n";
                errors+=1;
                std::cout<<"Length (original|bytes decoder|decoded):"<<a.getHeaderLength()+a.getLength()<<"|"<<Packet::getLengthOfPacket(bytes)<<"|"<<c.getLength()+c.getHeaderLength()<<std::endl;
                std::cout<<"Length original (header|packet):"<<(int)a.getHeaderLength()<<"|"<<a.getLength()<<std::endl;
                std::cout<<"Length decode (header|packet):"<<(int)c.getHeaderLength()<<"|"<<c.getLength()<<std::endl;
            }
            if(confirm and c.id!=5000-i){
                std::cout<<"Id is "<<c.id<<std::endl;
                errors+=1;
            }
            if(c.getClientId()!=i){
                std::cout<<"ClientId is "<<c.getClientId()<<std::endl;
                errors+=1;
            }
            int int32 = c.getInt32();
            if(int32!=9876){
                std::cout<<"int32 is "<<int32<<std::endl;
                errors+=1;
            }
            int uint16 = c.getUint16();
            if(uint16!=1234){
                std::cout<<"uint16 is "<<uint16<<std::endl;
                errors+=1;
            }
            double dbl = c.getDouble();
            if(dbl!=0.024035){
                std::cout<<"double is "<<dbl<<std::endl;
                errors+=1;
            }
            std::string text = c.getString();
            if(text!=std::string("Test")){
                std::cout<<"Text is \""<<text<<"\""<<std::endl;
                errors+=1;
            }
            boost::posix_time::ptime time2 = c.getTimeStamp();
            if(time2!=time){
                std::cout<<"Time is \""<<time2<<"\""<<std::endl;
                errors+=1;
            }
        }
        std::cout<<"Finished. There were "<<errors<<" errors\n";
    }
    else if(isFloodOut){
        bool confirm=false;
        int waitMs=1;
        unsigned int toSend=5000;
        Connection c("localhost",9876);
        c.waitMsResend=500;
        if(argc>2){
            std::stringstream s;s<<argv[2];
            s>>waitMs;
        }
        if(argc>3 and argv[3]==std::string("confirm"))
            confirm=true;
        if(argc>4){
            std::stringstream s;s<<argv[4];
            s>>c.waitMsResend;
        }
        if(argc>5){
            std::stringstream s;s<<argv[5];
            s>>toSend;
        }
        if(!c.connect()){
            std::cout<<"Cannot connect "<<errno<<"\n";
            return -1;
        }
        int out=0,in=0,bin=0,reout=0;
        boost::posix_time::time_duration minall=boost::posix_time::pos_infin,minto=boost::posix_time::pos_infin,minfrom=boost::posix_time::pos_infin;
        boost::posix_time::time_duration maxall=boost::posix_time::neg_infin,maxto=boost::posix_time::neg_infin,maxfrom=boost::posix_time::neg_infin;
        boost::posix_time::time_duration avgall=boost::posix_time::microseconds(0),avgto=boost::posix_time::microseconds(0),avgfrom=boost::posix_time::microseconds(0);
        bool loggedIn=false;
        int i=0;
        std::string text="Testing the protocol of The Seed Engine Network Component";
        c.setCallback([&](Packet& p, void* d)->void{
            DataPacket packet=p;
            std::string input=packet.getString();
            if(input=="login"){}//discard
            else if(input==text){
                in+=1;
                
                boost::posix_time::ptime send = packet.getTimeStamp(), receive = packet.getTimeStamp(), now = boost::posix_time::microsec_clock::universal_time();
                
                boost::posix_time::time_duration all=now-send;
                boost::posix_time::time_duration to=receive-send;
                boost::posix_time::time_duration from=now-receive;
                if(all>maxall)
                    maxall=all;
                if(all<minall)
                    minall=all;
                if(to>maxto)
                    maxto=to;
                if(to<minto)
                    minto=to;
                if(from>maxfrom)
                    maxfrom=from;
                if(from<minfrom)
                    minfrom=from;
                avgall+=all;
                avgto+=to;
                avgfrom+=from;
            }
            else{
                bin+=1;
                if(input.length()>1)
                    std::cout<<"Recvd \""<<input<<"\"\n";
            }
        },NULL);
        bool running=true;
        boost::thread sender([&]()->void{
            try{
            while(running){
                c.sendPackets(true);
                reout+=c.resendPackets();
                boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            }
            }
            catch(...){}
        });
        boost::thread reader([&]()->void{
            try{
            while(running){
                c.read(true); 
            }
            }
            catch(...){}
        });
        DataPacket login;
        login.confirmation_needed=true;
        login.putString("login");
        c.send(login);
        std::cout<<"Waiting to login\n";
        boost::posix_time::ptime start = boost::posix_time::microsec_clock::local_time();
        while(!c.isClientIdResolved() and boost::posix_time::microsec_clock::local_time()-boost::posix_time::seconds(4)<start)
            boost::this_thread::sleep(boost::posix_time::milliseconds(30));
        if(!c.isClientIdResolved()){
            std::cout<<"Couldn't login\n";
            reader.interrupt();
            sender.interrupt();
            return 1;
        }
        start=boost::posix_time::microsec_clock::local_time();
        for(i=0;i<toSend;i++){
            DataPacket p;
            p.confirmation_needed=confirm;
            p.putString(text);
            p.putTimeStamp(boost::posix_time::microsec_clock::universal_time());
            c.send(p);
            out++;
//            if(c.send(p))
//                out++;
            boost::this_thread::sleep(boost::posix_time::milliseconds(waitMs));
        }
        boost::posix_time::ptime stop=boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration duration=stop-start;
        std::cout<<"Sending lasted "<<duration<<"\n";
        if(waitMs<2)
            boost::this_thread::sleep(boost::posix_time::milliseconds(3000));
        else
            boost::this_thread::sleep(boost::posix_time::milliseconds(300));
        running=false;
        sender.interrupt();
        reader.interrupt();
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
        //reader.join();//;leave it
        std::cout<<"Sent "<<out<<" packets\n"
                <<"Resent "<<reout<<" packets\n"
                <<"Received "<<in<<" proper packets\n"
                <<"Received "<<bin<<" broken packets\n"
                <<"Packet loss: "<<100.0f-((float)in/(float)out)*100.0f<<"%\n\n";
        if(in>0){
            std::cout<<"Latency\n"
                    <<"Route\tMinimal\tMaximal\tAverage\n"
                    <<"Whole\t"<<minall<<"\t"<<maxall<<"\t"<<(avgall/in)<<"\n"
                    <<"To\t"<<minto<<"\t"<<maxto<<"\t"<<(avgto/in)<<"\n"
                    <<"From\t"<<minfrom<<"\t"<<maxfrom<<"\t"<<(avgfrom/in)<<"\n";
        }
    }
    else if(isFloodIn){
        int counter=0;
        SimpleCallbackServer server([&](SimpleCallbackServer* s, BaseClient* c, Packet& p)->bool{
            DataPacket packet = p;
            std::string msg=packet.getString();
            boost::posix_time::ptime sendTime=packet.getTimeStamp(), now=boost::posix_time::microsec_clock::universal_time();
            DataPacket resp;
            /*if(c->clientId==0 and msg=="login"){
                c->clientId=server.getNextFreeId();
                    /*if(server.connectedClients()>300 or c->clientId==0){//max 300 clients here
                        ProtocolPacket error;
                        error.setType(ProtocolPacket::SERVER_FULL);
                        server.send(c, error, false);
                        std::cout<<"Cannot login!\n";
                        return false;
                    }else{
                        ((ProtocolPacket)resp).setType(ProtocolPacket::CLIENTID);
                        //resp.setClientId(c->clientId);//should be done by sender automagically
                        if(((ProtocolPacket)resp).getType()!=ProtocolPacket::CLIENTID)
                            std::cout<<"Not set type!\n";
                        std::cout<<"Logged in!\n";
                    }
            }*/
            resp.putString(msg);
            if(msg!="login"){
                resp.putTimeStamp(sendTime);
                resp.putTimeStamp(now);
            }
            resp.confirmation_needed=packet.confirmation_needed;
            s->send(c,resp);
            counter++;
            if(counter%100==0)
                std::cout<<counter<<std::endl;
            return true;
        },9876);
        server.waitMsResend=100;
        if(argc>2){
            std::stringstream s;s<<argv[2];
            s>>server.waitMsResend;
        }
        server.waitMsDisconnect=3000;
        std::cout<<"Starting\n";
        if(!server.listen(1)){
            std::cout<<"Cannot listen, errno "<<errno<<"\n";
        }
    }
    else{
        ChatApp app(&argc, &argv);
        app.run();
    }
    return 0;
}

