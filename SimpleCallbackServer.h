/* 
 * File:   CallbackServer.h
 * Author: radek
 *
 * Created on 27 czerwiec 2013, 19:45
 */

#ifndef CALLBACKSERVER_H
#define	CALLBACKSERVER_H
#include "BaseServerConnection.h"
class SimpleCallbackServer : public BaseServerConnection<BaseClient>{
public:
    //typedef bool (*callback)(SimpleCallbackServer* server, BaseClient* client, Packet& packet);
    SimpleCallbackServer(std::function<bool(SimpleCallbackServer*, BaseClient*, Packet&)> onMessage, unsigned short port);
    virtual bool processPacket(BaseClient* client, Packet& packet);
    virtual ~SimpleCallbackServer();
private:
    std::function<bool(SimpleCallbackServer*, BaseClient*, Packet&)> processCall;
};

#endif	/* CALLBACKSERVER_H */

