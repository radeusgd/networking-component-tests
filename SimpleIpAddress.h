/* 
 * File:   SimpleIpAddress.h
 * Author: radek
 *
 * Created on 28 lipiec 2013, 21:01
 */

#ifndef SIMPLEIPADDRESS_H
#define	SIMPLEIPADDRESS_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

class SimpleIpAddress{
    sockaddr* socketAddress;
public:
    SimpleIpAddress();
    SimpleIpAddress(const SimpleIpAddress& other);
    SimpleIpAddress(const sockaddr* socketAddr);
    bool operator<(const SimpleIpAddress& other) const;
    bool operator>(const SimpleIpAddress& other) const;
    bool operator==(const SimpleIpAddress& other) const;
    void operator=(const SimpleIpAddress& other);
    sockaddr* getSocketAddress();
    virtual ~SimpleIpAddress();
};

#endif	/* SIMPLEIPADDRESS_H */

