/* 
 * File:   ChatApp.cpp
 * Author: radek
 * 
 * Created on 2 lipiec 2013, 11:57
 */

#include <gtk-3.0/gtk/gtk.h>

#include <boost/thread/thread.hpp>


#include "ChatApp.h"
#include "DataPacket.h"
#include "gtk-2.0/gdk/gdk.h"
#include "gtk-2.0/gtk/gtkscrolledwindow.h"
#include "gtk-2.0/gtk/gtkentry.h"
#include "gtk-2.0/gtk/gtktextview.h"
#include "gtk-2.0/gtk/gtktextiter.h"
#include "gtk-2.0/gtk/gtktextbuffer.h"
#include "gtk-2.0/gtk/gtkwidget.h"

ChatApp::ChatApp(int* argc, char*** argv) {
    connection=NULL;
    loggedIn=false;
    gtk_init(argc,argv);
    gdk_threads_init();
}

ChatApp::~ChatApp() {
}

void ChatApp::run(){
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    gtk_window_set_title (GTK_WINDOW (window), "Chat");
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);
    connection_form.container = gtk_grid_new();
    gtk_container_add (GTK_CONTAINER (window), connection_form.container);
    connection_form.init(this);
    gtk_widget_show_all(window);
    //gtk_widget_show(window);
    gdk_threads_enter();
    gtk_main();
    gdk_threads_leave();
}


void _onClickLogin(GtkWidget * widget, gpointer form){
    ((ChatApp::ConnectionForm*)form)->onClickLogin();
    
}

void ChatApp::ConnectionForm::init(ChatApp* app){
    this->app=app;
        GtkWidget* label = gtk_label_new("IP:");
        gtk_grid_attach (GTK_GRID (container), label, 0, 0, 1, 1);
        label = gtk_label_new("Port:");
        gtk_grid_attach (GTK_GRID (container), label, 0, 1, 1, 1);
        label = gtk_label_new("Nick:");
        gtk_grid_attach (GTK_GRID (container), label, 0, 2, 1, 1);
        ipEntry = gtk_entry_new();
        gtk_grid_attach (GTK_GRID (container), ipEntry, 1, 0, 2, 1);
        gtk_entry_set_text(GTK_ENTRY(ipEntry),"localhost");
        portEntry = gtk_entry_new();
        gtk_grid_attach (GTK_GRID (container), portEntry, 1, 1, 2, 1);
        gtk_entry_set_text(GTK_ENTRY(portEntry),"9876");
        nickEntry = gtk_entry_new();
        gtk_grid_attach (GTK_GRID (container), nickEntry, 1, 2, 2, 1);
        button = gtk_button_new_with_label("Login");
        g_signal_connect (button, "clicked", G_CALLBACK (_onClickLogin), gpointer(this));
        gtk_grid_attach (GTK_GRID (container), button, 0, 3, 3, 1);
        label = gtk_label_new("Status:");
        gtk_grid_attach (GTK_GRID (container), label, 0, 4, 1, 1);
        statusLabel = gtk_label_new("Not connected");
        gtk_grid_attach (GTK_GRID (container), statusLabel, 1, 4, 2, 1);
}

void _processPacket(Packet& packet, void* args){
    //std::cout<<"Incoming message!\n";
    DataPacket message = packet;
    ChatApp* app = (ChatApp*)args;
    boost::uint8_t msgId = message.getUint8();
    switch(msgId){
        case 0:
            if(!app->loggedIn and message.getUint8()==1){//if not already logged in and check packet integrity
                //TODO log in, switch to chat view
                app->loggedIn=true;
                std::cout<<"Successfull login!\n";
                gdk_threads_enter();
                gtk_label_set_text(GTK_LABEL(app->connection_form.statusLabel),"Logged in. Loading perspective");
                //switch widgets
                gtk_container_remove(GTK_CONTAINER (app->window), app->connection_form.container);
                app->chat.container = gtk_grid_new();
                gtk_container_add (GTK_CONTAINER (app->window), app->chat.container);
                app->chat.init(app);
                gtk_widget_show_all(app->window);
                gdk_threads_leave();
                app->chat.pinger = new boost::thread([](ChatApp* app)->void{
                    boost::this_thread::sleep(boost::posix_time::seconds(25));
                    while(app!=NULL and app->connection!=NULL){
                        DataPacket packet;
                        packet.confirmation_needed=true;
                        packet.putUint8(3);
                        app->connection->send(packet);
                        boost::this_thread::sleep(boost::posix_time::seconds(30));
                    }
                }, app);
            }
        break;
        case 1:
            if(!app->loggedIn){
                gtk_widget_set_sensitive(app->connection_form.button,true);
                gtk_label_set_text(GTK_LABEL(app->connection_form.statusLabel),"Login failed (error)");
                std::cout<<"Server responded: error!\n";
            }
        break;
        case 2:
            if(app->loggedIn){
                std::string nick = message.getString();
                boost::posix_time::ptime time = message.getTimeStamp();
                std::string txt = message.getString();
                std::string out = "["+boost::posix_time::to_simple_string(time)+"] "+nick+": "+txt+"\n";
                std::cout<<out;
                //GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(app->chat.textArea));
                //gtk_text_buffer_insert_at_cursor(buffer, out.c_str(), out.length());
                app->chat.messages[time]=out;
                app->chat.updateMessages();
            }
        break;
        case 3:
            std::cout<<".\n";
        break;
        default:
            std::cout<<"Message id: "<<(int)msgId<<"\n";
    }
}

void ChatApp::ConnectionForm::onClickLogin(){
    gtk_widget_set_sensitive(button,false);
    if(app->connection!=NULL){
        delete app->connection;
        app->connection=NULL;
    }
    if(timeout!=NULL){
        delete timeout;
        timeout=NULL;
    }
    if(app->reader!=NULL){
        delete app->reader;
        app->reader=NULL;
    }
    if(app->sender!=NULL){
        delete app->sender;
        app->sender=NULL;
    }
    gtk_label_set_text(GTK_LABEL(statusLabel),"Connecting");
    unsigned short port;
    {
        std::stringstream s;
        s<<gtk_entry_get_text(GTK_ENTRY(portEntry));
        s>>port;
        
    }
    app->connection = new Connection((char*)gtk_entry_get_text(GTK_ENTRY(ipEntry)),port);//TODO check ip if it's IP
    app->connection->waitMsResend=700;
    app->connection->setCallback(&_processPacket,(void*)app);
    if(!app->connection->connect()){
        gtk_widget_set_sensitive(app->connection_form.button,true);
        gtk_label_set_text(GTK_LABEL(app->connection_form.statusLabel),"Login failed (cannot connect)");
        delete app->connection;
        app->connection=NULL;
        return;
    }
    app->reader = new boost::thread([this]()->void{
        while(app->connection!=NULL){
            app->connection->read(true);
            boost::this_thread::sleep(boost::posix_time::milliseconds(5));
        }
    });
    app->sender = new boost::thread([this]()->void{
            try{
            while(app->connection!=NULL){
                app->connection->sendPackets(true);
                app->connection->resendPackets();
                boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            }
            }
            catch(...){}
        });
    timeout = new boost::thread([this]()->void{
        boost::this_thread::sleep(boost::posix_time::seconds(5));
        if(app->loggedIn==false){
            app->sender->interrupt();
            app->reader->interrupt();
            gtk_widget_set_sensitive(app->connection_form.button,true);
            gtk_label_set_text(GTK_LABEL(app->connection_form.statusLabel),"Login failed (timeout)");
            app->connection->stop();
        }
    });
    DataPacket login;
    login.confirmation_needed=true;
    login.putUint8(0);
    login.putString(std::string((char*)gtk_entry_get_text(GTK_ENTRY(nickEntry))));
    app->connection->send(login);
    /*if(!app->connection->send(login)){
        gtk_widget_set_sensitive(app->connection_form.button,true);
        gtk_label_set_text(GTK_LABEL(app->connection_form.statusLabel),"Login failed (cannot send login packet)");
        delete app->connection;
        app->connection=NULL;
        return;
    }*/
    gtk_label_set_text(GTK_LABEL(statusLabel),"Waiting for authorization");
}

void _onClickSend(GtkWidget * widget, gpointer chat){
    ((ChatApp::ChatWidget*)chat)->onClickSend();    
}

void ChatApp::ChatWidget::init(ChatApp* app){
    this->app=app;
    {
        /*GtkWidget* */textArea = gtk_text_view_new();
        gtk_text_view_set_editable(GTK_TEXT_VIEW(textArea),false);
        this->app->chat.scrolledWindow = gtk_scrolled_window_new(NULL, NULL);
        gtk_container_add(GTK_CONTAINER(this->app->chat.scrolledWindow), textArea);
        gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(this->app->chat.scrolledWindow),300);
        gtk_scrolled_window_set_min_content_width(GTK_SCROLLED_WINDOW(this->app->chat.scrolledWindow),400);
        gtk_grid_attach (GTK_GRID (container), this->app->chat.scrolledWindow, 0, 0, 10, 2);
    }
    
    GtkWidget* label = gtk_label_new("Message:");
    gtk_grid_attach (GTK_GRID (container), label, 0, 3, 1, 1);
    textInput = gtk_entry_new();
    gtk_grid_attach (GTK_GRID (container), textInput, 1, 3, 6, 1);
    GtkWidget* button = gtk_button_new_with_label("Send");
    g_signal_connect (button, "clicked", G_CALLBACK (_onClickSend), gpointer(this));
    gtk_grid_attach (GTK_GRID (container), button, 7, 3, 2, 1);
}

void ChatApp::ChatWidget::onClickSend(){
    std::string txt = (char*)gtk_entry_get_text(GTK_ENTRY(textInput));
    if(txt!="")
    {
        DataPacket message;
        message.confirmation_needed=true;
        message.putUint8(2);
        message.putTimeStamp(boost::posix_time::microsec_clock::local_time());
        message.putString(txt);
        app->connection->send(message);
        gtk_entry_set_text(GTK_ENTRY(textInput),"");
    }
    gtk_widget_grab_focus(textInput);
}

void ChatApp::ChatWidget::updateMessages(){
    if(messages.size()>20){
        for(std::map<boost::posix_time::ptime, std::string>::iterator i = messages.begin();messages.size()>20;i++)
            i=messages.erase(i);
    }
    std::string text;
    for(std::map<boost::posix_time::ptime, std::string>::iterator i = messages.begin();i!=messages.end();i++)
        text+=i->second;
    gdk_threads_enter();
    GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(app->chat.textArea));
    //gtk_text_buffer_insert_at_cursor(buffer, out.c_str(), out.length());
    gtk_text_buffer_set_text(buffer, text.c_str(),text.length());
    GtkAdjustment* adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scrolledWindow));
    gtk_adjustment_set_value(adj,gtk_adjustment_get_upper(adj));
    gtk_scrolled_window_set_vadjustment(GTK_SCROLLED_WINDOW(scrolledWindow),adj);
    gdk_threads_leave();
}