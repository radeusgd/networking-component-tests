#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1886523644/BaseServerConnection.o \
	${OBJECTDIR}/_ext/1886523644/ChatApp.o \
	${OBJECTDIR}/_ext/1886523644/ChatServer.o \
	${OBJECTDIR}/_ext/1886523644/Connection.o \
	${OBJECTDIR}/_ext/1886523644/DataPacket.o \
	${OBJECTDIR}/_ext/1886523644/Packet.o \
	${OBJECTDIR}/_ext/1886523644/ProtocolPacket.o \
	${OBJECTDIR}/_ext/1886523644/SimpleCallbackServer.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lboost_system -lboost_thread `pkg-config --libs glib-2.0` `pkg-config --libs gtk+-3.0`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/networking-component-tests

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/networking-component-tests: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/networking-component-tests ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1886523644/BaseServerConnection.o: /home/radek/NetBeansProjects/SimpleChat/BaseServerConnection.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/BaseServerConnection.o /home/radek/NetBeansProjects/SimpleChat/BaseServerConnection.cpp

${OBJECTDIR}/_ext/1886523644/ChatApp.o: /home/radek/NetBeansProjects/SimpleChat/ChatApp.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/ChatApp.o /home/radek/NetBeansProjects/SimpleChat/ChatApp.cpp

${OBJECTDIR}/_ext/1886523644/ChatServer.o: /home/radek/NetBeansProjects/SimpleChat/ChatServer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/ChatServer.o /home/radek/NetBeansProjects/SimpleChat/ChatServer.cpp

${OBJECTDIR}/_ext/1886523644/Connection.o: /home/radek/NetBeansProjects/SimpleChat/Connection.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/Connection.o /home/radek/NetBeansProjects/SimpleChat/Connection.cpp

${OBJECTDIR}/_ext/1886523644/DataPacket.o: /home/radek/NetBeansProjects/SimpleChat/DataPacket.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/DataPacket.o /home/radek/NetBeansProjects/SimpleChat/DataPacket.cpp

${OBJECTDIR}/_ext/1886523644/Packet.o: /home/radek/NetBeansProjects/SimpleChat/Packet.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/Packet.o /home/radek/NetBeansProjects/SimpleChat/Packet.cpp

${OBJECTDIR}/_ext/1886523644/ProtocolPacket.o: /home/radek/NetBeansProjects/SimpleChat/ProtocolPacket.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/ProtocolPacket.o /home/radek/NetBeansProjects/SimpleChat/ProtocolPacket.cpp

${OBJECTDIR}/_ext/1886523644/SimpleCallbackServer.o: /home/radek/NetBeansProjects/SimpleChat/SimpleCallbackServer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1886523644
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1886523644/SimpleCallbackServer.o /home/radek/NetBeansProjects/SimpleChat/SimpleCallbackServer.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0`   -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/networking-component-tests

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
