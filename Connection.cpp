/* 
 * File:   Connection.cpp
 * Author: radek
 * 
 * Created on 26 czerwiec 2013, 22:34
 */

#include "Connection.h"
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "ProtocolPacket.h"
#include "concurrent_set_simple.h"
#include <sstream>
#include <fcntl.h>

Connection::Connection(const char* ip, unsigned short port){// :socket(io_service,udp::endpoint(udp::v4(), 0)), worker(io_service){    
//     udp::resolver resolver(io_service);
//     std::stringstream s;s<<port;
//     udp::resolver::query query(udp::v4(), ip, s.str().c_str());
//     endpoint = *resolver.resolve(query);
     currentId=0;
     latestConfirmedMessage=0;
     //confirmations=0;
     lastMessageTime=boost::posix_time::second_clock::local_time()-boost::posix_time::hours(12);
     onPacket=NULL;
     serverIp=ip;
     serverPort=port;
     clientId=0;
}

bool Connection::connect(){
    struct addrinfo hints, *res;
    // first, load up address structs with getaddrinfo():
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;//AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    std::stringstream s;s<<serverPort;
    getaddrinfo(serverIp, s.str().c_str(), &hints, &res);
    // make a socket:
    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    // connect!
    if(::connect(sockfd, res->ai_addr, res->ai_addrlen)==-1)
        return false;
    return true;
}

Connection::~Connection() {
    stop();
}

void Connection::send(Packet& packet, bool raw){
    if(packet.confirmation_needed and !raw){
        {
            boost::mutex::scoped_lock lock(currentIdMutex);
            packet.id=currentId;
            currentId++;
            if(currentId>65000)
                currentId=0;
        }
        //std::cout<<currentId<<"\n";
        confirmablePackets[packet.id]=packet;
    }
    packet.setClientId(clientId);
    //if(packet.sendTime == boost::posix_time::neg_infin)
        packet.sendTime=boost::posix_time::microsec_clock::local_time();
    packetsToSend.push(packet);
    /*if(sendto(sockfd,
            packet.getBytes(),
            packet.getHeaderLength()+packet.getLength(),
            0,
            endpoint.getSocketAddress(),
            sizeof(sockaddr))==packet.getHeaderLength()+packet.getLength())*/
    /*if(::send(sockfd,
            packet.getBytes(),
            packet.getHeaderLength()+packet.getLength(),
            0)==packet.getHeaderLength()+packet.getLength())
        return true;
    return false;*/
}

void Connection::read(bool blocking){
/*        _data = new boost::uint8_t[Packet::max_length];
        socket.async_receive_from(
          boost::asio::buffer(_data, Packet::max_length), _sender,
          boost::bind(&Connection::handle_receive_from, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred)); //start receiving
        if(blocking){
            io_service.run();
        }
        else{
            
            boost::thread* t = new boost::thread([this]()->void{
                io_service.run();
            });
        }*/
    int byte_count;
    sockaddr addr;
    //SimpleIpAddress _sender;
    if(!blocking)
        fcntl(sockfd, F_SETFL, O_NONBLOCK);
    //char ipstr[INET6_ADDRSTRLEN];
    //socklen_t fromlen = sizeof(addr);
    boost::uint8_t* _data = new boost::uint8_t[Packet::max_length];
    byte_count = recv(sockfd, _data, Packet::max_length, 0);//byte_count = recvfrom(sockfd, _data, sizeof(_data), 0, &addr, &fromlen);
    //_sender = SimpleIpAddress(&addr);
    if(byte_count<1)
        return;
    //printf("recv()'d %d bytes of data in buf\n", byte_count);
    if(byte_count<Packet::getMinimalHeaderLength()){
        printf("Packet invalid (doesn't contain header)\n");
        return;
    }
    if(byte_count<Packet::getLengthOfPacket(_data)){
        printf("Packet invalid (not all data received)\n");
        return;
    }
    Packet received_packet(_data);
    delete [] _data;
        if(((ProtocolPacket)received_packet).getType()==ProtocolPacket::CONFIRMATION){
            ProtocolPacket confirmation = received_packet;
            //confirmablePackets.erase(confirmation.getUint16());
            boost::int16_t latestConfirmed = confirmation.getUint16();
            confirmablePackets.erase(latestConfirmed);
            boost::int16_t r_confirmations=confirmation.getUint16();
            for(char i=0;i<16;i++){
                if(r_confirmations&(1<<i)){
                    confirmablePackets.erase(latestConfirmed-i);
                }
                else{
                    if(confirmablePackets.find(latestConfirmed-i)!=confirmablePackets.end()){
                        /*sendto(sockfd,
                            confirmablePackets[latestConfirmed-i].getBytes(),
                            confirmablePackets[latestConfirmed-i].getHeaderLength()+confirmablePackets[latestConfirmed-i].getLength(),
                            0,
                            endpoint.getSocketAddress(),
                            sizeof(addr));*/
                        /*::send(sockfd,
                            confirmablePackets[latestConfirmed-i].getBytes(),
                            confirmablePackets[latestConfirmed-i].getHeaderLength()+confirmablePackets[latestConfirmed-i].getLength(),
                            0);*/
                        send(confirmablePackets[latestConfirmed-i],true);
                    }
                }
            }
            lastMessageTime = boost::posix_time::special_values::neg_infin;
            return;
        }
    if(clientId==0 and received_packet.getClientId()!=0){//if(((ProtocolPacket)received_packet).getType()==ProtocolPacket::CLIENTID){
        clientId=received_packet.getClientId();
        std::cout<<"Client ID is now "<<clientId<<"\n";
    }
    if(received_packet.confirmation_needed){
        if(confirmations.find(received_packet.id)){//if(confirmations[received_packet.id]==true){
            //std::cout<<received_packet.id<<" rep\n";
            return;//packet already received
        }
        sendConfirmation(received_packet.id);
    }
    if(onPacket)
        onPacket(received_packet,args);
//    delete [] _data;
}

void Connection::sendConfirmation(boost::uint16_t confirmedId){
    ProtocolPacket confirmation;
    confirmation.confirmation_needed=false;
    confirmation.setType(ProtocolPacket::CONFIRMATION);
    confirmations.insert(confirmedId);//confirmations[confirmedId]=true;
    {//update confirmations
        if(confirmedId>=latestConfirmedMessage){
            latestConfirmedMessage = confirmedId;
        }
    }
    //confirmation.putUint32(latestConfirmedMessage);
    //confirmation.putInt16(confirmations);
    confirmation.putUint16(confirmedId);
    boost::uint16_t confirms=0;
    for(int i=0;i<16;i++)
        if(confirmations.find(confirmedId-i))//if(confirmations[confirmedId-i])
            confirms|=(1<<i);
    confirmation.putUint16(confirms);
    send(confirmation);
    clearOldConfirmations();
}

void Connection::setCallback(std::function<void(Packet&, void*)> function, void* arguments){
    onPacket=function;
    args=arguments;
}

void Connection::stop(){
    close(sockfd);
    /*try{
    if(!io_service.stopped())
        io_service.stop();
    if(socket.is_open())
        socket.close();
    }
    catch(...){}*/
}

unsigned int Connection::resendPackets(){
    unsigned int count=0;
    concurrent_map<boost::uint16_t,Packet>::lock lock(confirmablePackets);
    boost::posix_time::time_duration waiter = boost::posix_time::milliseconds(waitMsResend);//TODO
    for(std::map<boost::uint16_t,Packet>::iterator i=lock.getMap().begin();i!=lock.getMap().end();i++){//for(std::map<boost::uint16_t,Packet>::iterator i=confirmablePackets.begin();i!=confirmablePackets.end();i++){
        boost::posix_time::time_duration duration = (boost::posix_time::microsec_clock::local_time())-(i->second.sendTime);
        if(duration>waiter){
            send(i->second, true);
        //    std::cout<<i->second.sendTime<<" "<<duration<<" "<<waiter<<"\n";
            count++;
        }
        if((signed long int)currentId-5000>i->second.id  or (currentId>9000 and currentId<32000 and i->second.id>40000))//packet too old, give up
            confirmablePackets.erase(i);
    }
    return count;
}

void Connection::sendPackets(bool blocking){
    if(packetsToSend.empty())
        return;
    do{
        Packet p=packetsToSend.front();
        int length=p.getHeaderLength()+p.getLength();
        boost::uint8_t* bytes=p.getBytes();
        if(::send(sockfd,
            bytes,
            length,
            0)==length)
                packetsToSend.pop();
        delete [] bytes;
        boost::this_thread::sleep(boost::posix_time::microseconds(50));
    }while(blocking and !packetsToSend.empty());
}

bool Connection::isClientIdResolved(){
    return (clientId!=0);
}

void Connection::clearOldConfirmations(){
    concurrent_set<boost::uint16_t>::lock lock(confirmations);
    for(auto i : lock.getSet()){
        if(i<latestConfirmedMessage-10000 or (i>40000 and latestConfirmedMessage>9000 and latestConfirmedMessage<32000))
            i=lock.getSet().erase(i);
    }
}