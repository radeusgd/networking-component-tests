#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=simplechat
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/simplechat
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=simplechat.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/simplechat.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=simplechat
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/simplechat
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=simplechat.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/simplechat.tar
# Debug_Server configuration
CND_PLATFORM_Debug_Server=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug_Server=dist/Debug_Server/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug_Server=simplechat
CND_ARTIFACT_PATH_Debug_Server=dist/Debug_Server/GNU-Linux-x86/simplechat
CND_PACKAGE_DIR_Debug_Server=dist/Debug_Server/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug_Server=simplechat.tar
CND_PACKAGE_PATH_Debug_Server=dist/Debug_Server/GNU-Linux-x86/package/simplechat.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
