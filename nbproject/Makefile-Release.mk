#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/BaseServerConnection.o \
	${OBJECTDIR}/ChatApp.o \
	${OBJECTDIR}/ChatServer.o \
	${OBJECTDIR}/Connection.o \
	${OBJECTDIR}/DataPacket.o \
	${OBJECTDIR}/Packet.o \
	${OBJECTDIR}/ProtocolPacket.o \
	${OBJECTDIR}/SimpleCallbackServer.o \
	${OBJECTDIR}/SimpleIpAddress.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lboost_system -lboost_thread `pkg-config --libs glib-2.0` `pkg-config --libs gtk+-3.0`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplechat

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplechat: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplechat ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/BaseServerConnection.o: BaseServerConnection.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/BaseServerConnection.o BaseServerConnection.cpp

${OBJECTDIR}/ChatApp.o: ChatApp.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/ChatApp.o ChatApp.cpp

${OBJECTDIR}/ChatServer.o: ChatServer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/ChatServer.o ChatServer.cpp

${OBJECTDIR}/Connection.o: Connection.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/Connection.o Connection.cpp

${OBJECTDIR}/DataPacket.o: DataPacket.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/DataPacket.o DataPacket.cpp

${OBJECTDIR}/Packet.o: Packet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/Packet.o Packet.cpp

${OBJECTDIR}/ProtocolPacket.o: ProtocolPacket.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/ProtocolPacket.o ProtocolPacket.cpp

${OBJECTDIR}/SimpleCallbackServer.o: SimpleCallbackServer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/SimpleCallbackServer.o SimpleCallbackServer.cpp

${OBJECTDIR}/SimpleIpAddress.o: SimpleIpAddress.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/SimpleIpAddress.o SimpleIpAddress.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-3.0` -std=c++11  -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/simplechat

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
