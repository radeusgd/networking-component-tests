/* 
 * File:   ProtocolPacket.cpp
 * Author: radek
 * 
 * Created on 27 czerwiec 2013, 21:01
 */

#include "ProtocolPacket.h"

ProtocolPacket::ProtocolPacket() : DataPacket() {}

ProtocolPacket::ProtocolPacket(const Packet& orig) : DataPacket(orig) {}

ProtocolPacket::ProtocolPacket(boost::uint8_t* data) : DataPacket(data){}

ProtocolPacket::~ProtocolPacket() {
}

void ProtocolPacket::setType(Type t){
    //flags &=(1<<0);//can have multiple types at once
    flags |=(1<<t);
}

ProtocolPacket::Type ProtocolPacket::getType(){
    for(char i=1;i<8;i++){
        if(flags & (1<<i))
            return (Type)i;//return only first type :/
    }
}