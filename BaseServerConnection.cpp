/* 
 * File:   ServerConnection.cpp
 * Author: radek
 * 
 * Created on 27 czerwiec 2013, 10:39
 */


#include "BaseServerConnection.h"


BaseClient::BaseClient(SimpleIpAddress& sender, boost::uint16_t clientId){
    endpoint=sender;
    //confirmations=0;
    currentId=0;
    lastMessageTime=boost::posix_time::neg_infin;
    latestConfirmedMessage=0;
    this->clientId=clientId;
}

