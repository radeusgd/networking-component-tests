/* 
 * File:   Packet.h
 * Author: radek
 *
 * Created on 25 czerwiec 2013, 21:55
 */

#ifndef PACKET_H
#define	PACKET_H
#include <vector>
#include <boost/integer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class Packet {
public:
    Packet();//creates empty packet
    Packet(boost::uint16_t clientId);
    Packet(boost::uint8_t* data);//creates packet from data
    Packet(const Packet& orig);
    void operator=(const Packet& orig);
    
    boost::uint16_t getClientId();
    void setClientId(boost::uint16_t id);
    boost::uint8_t* getBytes();//returns whole data needed to be sent, you need to delete it for yourself!
    unsigned short getLength();
    unsigned char getHeaderLength();
    static const unsigned char getMinimalHeaderLength();
    
    bool confirmation_needed;
    boost::uint16_t id;
    boost::posix_time::ptime sendTime;
    
    virtual ~Packet();
    static unsigned short getLengthOfPacket(boost::uint8_t* data);
    
    static const unsigned int max_length=1023;//TODO guard packet not to be longer
    static int is_lil_endian();
protected:
    std::vector<boost::uint8_t> data;
    boost::uint8_t flags;
    
    boost::uint16_t clientId;
};

#endif	/* PACKET_H */

