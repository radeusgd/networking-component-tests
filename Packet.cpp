/* 
 * File:   Packet.cpp
 * Author: radek
 * 
 * Created on 25 czerwiec 2013, 21:55
 */

#include "Packet.h"

int Packet::is_lil_endian(void)
{
    union {
        boost::uint32_t i;
        boost::uint8_t c[4];
    } bint = {0x01020304};

    return !(bint.c[0] == 1); 
}

Packet::Packet() {
    confirmation_needed=false;
    flags=0;
    sendTime = boost::posix_time::neg_infin;
    clientId=0;
    id=0;
}

Packet::Packet(boost::uint16_t clientId) : Packet(){
    this->clientId=clientId;
}

Packet::Packet(boost::uint8_t* bytes) {
    sendTime = boost::posix_time::neg_infin;//sending timestamp has to be done on higher layer
    unsigned short len;
    {
        boost::uint8_t length[2];
        if(Packet::is_lil_endian()){
            length[0]=bytes[0];
            length[1]=bytes[1];
        }
        else{
            length[0]=bytes[1];
            length[1]=bytes[0];
        }
        len = *(boost::uint16_t*)length;
    }
    flags=bytes[2];
    confirmation_needed = ((1<<0)&flags);
    {
        boost::uint8_t id[2];
        if(Packet::is_lil_endian()){
            id[0]=bytes[3];
            id[1]=bytes[4];
        }
        else{
            id[0]=bytes[4];
            id[1]=bytes[3];
        }
        clientId = *(boost::uint16_t*)id;
    }
    unsigned short diff=5;
    if(confirmation_needed)
    {
        boost::uint8_t msgId[2];
        if(Packet::is_lil_endian()){
            msgId[0]=bytes[5];
            msgId[1]=bytes[6];
            //msgId[2]=bytes[5];
            //msgId[3]=bytes[6];
        }
        else{
//            msgId[0]=bytes[6];
//            msgId[1]=bytes[5];
//            msgId[2]=bytes[4];
//            msgId[3]=bytes[3];
            msgId[0]=bytes[6];
            msgId[1]=bytes[5];
        }
        this->id=*(boost::uint16_t*)msgId;
        //std::cout<<(int)(*msgId)<<"\n";
        diff=7;
    }
    for(unsigned short i=0;i<len;i++){
        data.push_back(bytes[diff+i]);
    }
    //std::cout<<"Data "<<data.size()<<"\n";
    //std::cout.flush();
}

Packet::Packet(const Packet& orig) {
    confirmation_needed=orig.confirmation_needed;
    data=orig.data;
    id=orig.id;
    flags=orig.flags;
    sendTime=orig.sendTime;
    clientId=orig.clientId;
}

void Packet::operator =(const Packet& orig){
    confirmation_needed=orig.confirmation_needed;
    data=orig.data;
    id=orig.id;
    flags=orig.flags;
    sendTime=orig.sendTime;
    clientId=orig.clientId;
}

Packet::~Packet() {
}


/*inline */unsigned short Packet::getLength(){
    return data.size();
}

boost::uint8_t* Packet::getBytes(){
    boost::uint8_t* bytes;// = new boost::uint8_t[2+1+4+data.size()];//TODO add timestamp
    if(confirmation_needed)
        bytes = new boost::uint8_t[2+1+2+2+data.size()];
    else
        bytes = new boost::uint8_t[2+1+2+data.size()];
    {
        unsigned short len = getLength();
        boost::uint8_t* length = (boost::uint8_t*)&len;
        if(Packet::is_lil_endian()){
            bytes[0]=length[0];
            bytes[1]=length[1];
        }
        else{
            bytes[0]=length[1];
            bytes[1]=length[0];
        }
    }
    //flags=0;
    flags&=~(confirmation_needed<<0);
    flags|=(confirmation_needed<<0);
    bytes[2]=flags;
    {
        boost::uint8_t* id = (boost::uint8_t*)&clientId;
        if(Packet::is_lil_endian()){
            bytes[3]=id[0];
            bytes[4]=id[1];
        }
        else{
            bytes[3]=id[1];
            bytes[4]=id[0];
        }
    }
    unsigned short diff=5;
    if(confirmation_needed){
        boost::uint8_t* msgId = (boost::uint8_t*)&id;
//        std::cout<<*(boost::uint16_t*)(boost::uint8_t*)&id<<"\n";
        if(Packet::is_lil_endian()){
            bytes[5]=msgId[0];
            bytes[6]=msgId[1];
            //bytes[4]=msgId[5];
            //bytes[5]=msgId[6];
        }
        else{
            //bytes[2]=msgId[6];
            //bytes[3]=msgId[5];
            //bytes[4]=msgId[4];
            //bytes[5]=msgId[3];
            bytes[5]=msgId[1];
            bytes[6]=msgId[0];
        }
        diff=7;
    }
    for(unsigned short i=0;i<data.size();i++){
        bytes[diff+i]=data[i];
    }
    return bytes;
}

const unsigned char Packet::getMinimalHeaderLength(){
    return 2+1+2;
}

unsigned char Packet::getHeaderLength(){
    if(confirmation_needed)
        return 2+1+2+2;
    return 2+1+2;
}

unsigned short Packet::getLengthOfPacket(boost::uint8_t* data){
        boost::uint8_t length[2];
        if(Packet::is_lil_endian()){
            length[0]=data[0];
            length[1]=data[1];
        }
        else{
            length[0]=data[1];
            length[1]=data[0];
        }
        unsigned short len = *length;
        len+=2+1+2;
    if((1<<0&data[2]))
        len+=2;
    return len;
}

boost::uint16_t Packet::getClientId(){
    return clientId;
}

void Packet::setClientId(boost::uint16_t cid){
    clientId=cid;
}