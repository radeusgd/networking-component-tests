/* 
 * File:   concurrent_map.h
 * Author: radek
 *
 * Created on 2 sierpień 2013, 16:52
 */

#ifndef CONCURRENT_MAP_H
#define	CONCURRENT_MAP_H

#include <map>
#include <boost/thread.hpp>

template<typename Key, typename Data>
class concurrent_map
{
private:
    std::map<Key, Data> the_map;
    mutable boost::mutex the_mutex;
public:
    //class iterator : public std::map<Key, Data>::iterator{};
    typedef typename std::map<Key, Data>::iterator iterator;
    iterator begin()
    {
        boost::mutex::scoped_lock lock(the_mutex);
        return the_map.begin();
    }
    
    iterator end()
    {
        boost::mutex::scoped_lock lock(the_mutex);
        return the_map.end();
    }

    Data& operator[](const Key& k){
        boost::mutex::scoped_lock lock(the_mutex);
        return the_map[k];
    }
    
    iterator find(const Key& k){
        boost::mutex::scoped_lock lock(the_mutex);
        return the_map.find(k);
    }
    
    size_t erase(const Key& k){
        boost::mutex::scoped_lock lock(the_mutex);
        return the_map.erase(k);
    }
    
    iterator erase(const iterator& i){
        boost::mutex::scoped_lock lock(the_mutex);
        return the_map.erase(i);
    }
    
    size_t size(){
        boost::mutex::scoped_lock lock(the_mutex);
        return the_map.size();
    }
    
    void insert(std::pair<Key, Data> p){
        boost::mutex::scoped_lock lock(the_mutex);
        the_map.insert(p);
    }
    
    class lock{
        boost::mutex::scoped_lock slock;
        std::map<Key, Data>& m;
    public:
        lock(concurrent_map& map) : slock(map.the_mutex), m(map.the_map){}
        std::map<Key, Data>& getMap(){
            return m;
        }
    };
    
};



#endif	/* CONCURRENT_MAP_H */

