/* 
 * File:   ChatServer.h
 * Author: radek
 *
 * Created on 1 lipiec 2013, 17:18
 */

#ifndef CHATSERVER_H
#define	CHATSERVER_H

#include "BaseServerConnection.h"

class ChatClient : public BaseClient{
public:
    ChatClient(SimpleIpAddress endpoint, boost::uint16_t id);
    std::string nickname;
}; 

class ChatServer : public BaseServerConnection<ChatClient>{
public:
    ChatServer(unsigned short port=9887);
    virtual ~ChatServer();
    bool processPacket(ChatClient* client, Packet& packet);
    void sendToAll(Packet& packet);
private:

};

#endif	/* CHATSERVER_H */

